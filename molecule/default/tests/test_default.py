import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_uboot_image_exists(host):
    assert host.file('/var/lib/tftpboot/boot/env-isrc-vme.scr').is_file
    env_mac = host.file('/var/lib/tftpboot/boot/env-AA:AA:AA:42:42:42.scr')
    assert env_mac.is_symlink
    assert env_mac.linked_to == '/var/lib/tftpboot/boot/env-isrc-vme.scr'
