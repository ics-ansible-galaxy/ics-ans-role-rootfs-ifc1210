ics-ans-role-rootfs-ifc1210
===========================

Ansible role to install the EEE ifc1210 root file system to boot VME IOCs.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
rootfs_ifc1210_version: 180824
rootfs_ifc1210_archive: "ifc1210-ess-rootfs-{{ rootfs_ifc1210_version }}.tar.bz2"
rootfs_ifc1210_base_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/EEE/eldk"
rootfs_ifc1210_archive_url: "{{ rootfs_ifc1210_base_url }}/{{ rootfs_ifc1210_archive }}"

rootfs_ifc1210_kernel_default: uImage--3.14+git0+09424cee64_3428de7103-r0-ifc1210-20150917083734.bin
rootfs_ifc1210_kernel_images:
  - uImage--3.14.40+git0+09424cee64_7ce37e045a-r0-ifc1210-20160428191749.bin
  - uImage--3.14+git0+09424cee64_3428de7103-r0-ifc1210-20150917083734.bin
  - uImage--3.14.40_git0_09424cee64_7ce37e045a-r0-ifc1210-20160428191749.bin
  - uImage--3.14.40_git0_09424cee64_7ce37e045a-r0-ifc1210-20170123123858.bin
rootfs_ifc1210_dtb:
  - uImage-p2020ifc1210.dtb
  - uImage--3.14+git0+09424cee64_3428de7103-r0-p2020ifc1210-20150803122042.dtb
rootfs_ifc1210_bitfiles:
  - "FPGA-bitfiles/ifc1210-default/ifc1210_basic_current_IO.bit"
  - "FPGA-bitfiles/ifc1210-default/ifc1210_iocstandard_current_CENTRAL.bit"
  - "FPGA-bitfiles/ifc1210-scope/ifc1210_top_central_scope_dtacq_a0_20160202.bit"
rootfs_ifc1210_env_scripts:
  - host: isrc-vme
    mac: "AA:AA:AA:42:42:42"
    bootfile: "uImage--3.14.40_git0_09424cee64_7ce37e045a-r0-ifc1210-20170123123858.bin"
    fdtfile: "uImage--3.14+git0+09424cee64_3428de7103-r0-p2020ifc1210-20150803122042.dtb"
    fpgace: "ifc1210_top_central_scope_dtacq_a0_20160202.bit"
    fpgaio: "ifc1210_basic_current_IO.bit"

rootfs_ifc1210_dir: /export/nfsroots/ifc1210/{{ rootfs_ifc1210_version }}/rootfs
rootfs_ifc1210_eee_server: "{{ ansible_default_ipv4.address }}"
rootfs_ifc1210_nfs_mountpoints:
  - src: "{{ rootfs_ifc1210_eee_server }}:/export/epics"
    mountpoint: /opt/epics
    rw: ro
  - src: "{{ rootfs_ifc1210_eee_server }}:/export/startup"
    mountpoint: /opt/startup
    rw: ro
  - src: "{{ rootfs_ifc1210_eee_server }}:/export/nonvolatile"
    mountpoint: /opt/nonvolatile
    rw: rw
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rootfs-ifc1210
```

License
-------

BSD 2-clause
